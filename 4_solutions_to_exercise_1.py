# -*- coding: utf-8 -*-
"""
Created on Wed May 12 15:24:38 2021

@author: user
"""

import numpy as np
import pickle

# %%

x_int_16 = np.zeros( 1000 , dtype=np.int16 )

print( 'size of saved structure: ' + str(len(pickle.dumps( x_int_16 , -1))) )

# 1 byte = 8 bits
# 16-bit number -> 2 bytes

r_int_16 = np.random.randint(2**15, size=1000, dtype=np.int16)
print( 'size of saved structure: ' + str(len(pickle.dumps( r_int_16 , -1))) )
# random values: same size as zeros

# %%

x_int_32 = np.zeros( 1000 , dtype=np.int32 )

print( 'size of saved structure: ' + str(len(pickle.dumps( x_int_32 , -1))) )

# 1 byte = 8 bits
# 32-bit number -> 4 bytes

# %%

x_int_64 = np.zeros( 1000 , dtype=np.int64 )

print( 'size of saved structure: ' + str(len(pickle.dumps( x_int_64 , -1))) )

# 1 byte = 8 bits
# 64-bit number -> 8 bytes


# %%

x_float_32 = np.zeros( 1000 , dtype=np.float32)

print( 'size of saved structure: ' + str(len(pickle.dumps( x_float_32 , -1))) )

# 1 byte = 8 bits
# 32-bit number -> 4 bytes

# %%

x_float_64 = np.zeros( 1000 , dtype=np.float64)

print( 'size of saved structure: ' + str(len(pickle.dumps( x_float_64 , -1))) )

# 1 byte = 8 bits
# 32-bit number -> 4 bytes


# %%
    
r_int_16 = np.random.randint(2**15, size=1000, dtype=np.int16)
print( 'size of saved structure: ' + str(len(pickle.dumps( r_int_16 , -1))) )
'''
# ISN'T WORKING!'
r_float_16 = np.random.random(2**20, size=1000, dtype=np.float16)
print( 'size of saved structure: ' + str(len(pickle.dumps( r_float_16 , -1))) )
'''
# 1 byte = 8 bits
# 32-bit number -> 4 bytes